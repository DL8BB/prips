#ifndef __prips_h__
#define __prips_h__

/*
 * Copyright (C) 2001-2003  Daniel Kelly
 * Copyright (C) 2009  Peter Pentchev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifdef HAVE_POSIX
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#endif

#include <arpa/inet.h>

#ifndef HAVE_UINT32_T
#ifdef INT32_T
typedef unsigned INT32_T uint32_t;
#warning Using the Makefile-defined INT32_T as int
#else
typedef unsigned int uint32_t;
#warning Just guessing, using unsigned int for uint32_t
#endif
#endif

#define BUF_SIZE 128

#define IPQUAD(addr) \
        ((unsigned char *)&addr)[3], \
        ((unsigned char *)&addr)[2], \
        ((unsigned char *)&addr)[1], \
        ((unsigned char *)&addr)[0]

uint32_t numberize(const char *addr);
const char *denumberize(uint32_t addr);
const char *cidrize(uint32_t start, uint32_t end);
uint32_t add_offset(const char *addr, int offset);
uint32_t set_bits_from_right(int bits);
int count_on_bits(uint32_t number);
char get_class(uint32_t address);

#endif /* __prips_h__ */
